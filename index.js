var express = require('express');
var app = express();
const bodyParser = require('body-parser');
const morgan = require("morgan");
const path = require("path");
const fs = require("fs");

const hostPort = 8080;
const serverIp = "localhost";
const directoryPath = path.join(__dirname, "files");

var files = [];
var fileExtensions = [ 'log', 'txt', 'json', 'yaml', 'xml', 'js' ];

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.get("/", (req, res) => {
    res.status(200).send(JSON.stringify("File server"));   
});

app.use(function (req, res, next) {    
    console.log(req.originalUrl, new Date().toISOString());
    next();
});


app.get("/api/files", function (req, res) {
    let fileObj = {};

    files = getFiles();
    fileObj.message = "Success";
    fileObj.files = files;        

    let jsonObj = JSON.stringify(fileObj);

    res.status(200).send(jsonObj);
});

app.get("/api/files/:fName", function (req, res) {
    let file = directoryPath + '/' + req.params.fName;
    let fileObj = {};
    let fileNotFound = {};

    fs.readFile(file, 'utf8', function (err, data) {
        if (err) {
            fileNotFound.message = "No file with "+ req.params.fName + " filename found";

            res.status(400).send(JSON.stringify(fileNotFound));
            console.log(err);
        } else {
            fileObj.message = "Success";
            fileObj.filename = req.params.fName;
            fileObj.data = data;
            fileObj.extension = path.extname(file);            
            let stats = fs.statSync(file);
            let mtime = stats.mtime;
            fileObj.uploadedDate = mtime;

            res.status(200).send(JSON.stringify(fileObj));
        }
    });
});

app.post('/api/files', function (req, res) {    
    let fileName = req.body.filename;
    let content = req.body.content;
    let fullName = directoryPath + '/' + fileName;

    let dotIndex = fileName.lastIndexOf(".") - 1;
    let fExtension = fileName.substr(dotIndex + 2, fileName.length);
    let isAllowFileExt = fileExtensions.indexOf(fExtension) > -1;
    
    fs.writeFile(fullName, content, function (err) {
        if (err){

        }else{
            let fileObj = {};
            fileObj.message = "File created successfully";

            res.status(200).send(JSON.stringify(fileObj));
        }
      });
        
});

function getFiles(){
   return fs.readdirSync(directoryPath);
}

app.listen(hostPort, serverIp, () => {
    console.log('Server started at 8080');
});